package fr.devlapp.testeurosport

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import fr.devlapp.core.model.Data
import fr.devlapp.core.model.Data.Story
import fr.devlapp.core.model.Data.Video
import fr.devlapp.core.model.RepositoryException
import fr.devlapp.testeurosport.MainViewModel.MainViewState.*
import fr.devlapp.testeurosport.ui.story.StoryActivity
import fr.devlapp.testeurosport.ui.theme.Grey
import fr.devlapp.testeurosport.ui.video.VideoActivity
import fr.devlapp.testeurosport.ui.views.StoryCard
import fr.devlapp.testeurosport.ui.views.VideoCard
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            viewModel.viewEvent.collect { viewState ->
                setContent {
                    when (viewState) {
                        is MainViewData -> List(viewState.listData)
                        is MainViewError -> Error(viewState.error)
                        is MainViewLoading -> Loading()
                    }
                }
            }
        }

        viewModel.getMainData()
    }

    private fun openStory(story: Story) {
        startActivity(StoryActivity.getIntent(this, story))
    }

    private fun openVideo(video: Video) {
        startActivity(VideoActivity.getIntent(this, video))
    }

    @Composable
    private fun List(listData: List<Data>) {
        LazyColumn(
            modifier = Modifier.background(Grey),
            contentPadding = PaddingValues(0.dp, 8.dp, 0.dp, 8.dp)
        ) {
            items(listData) { mainData ->
                when (mainData) {
                    is Story -> StoryCard(mainData, ::openStory)
                    is Video -> VideoCard(mainData, ::openVideo)
                }
            }
        }
    }

    @Composable
    private fun Error(error: RepositoryException) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = error.type.name)
        }
    }

    @Composable
    private fun Loading() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(modifier = Modifier.size(32.dp))
        }
    }
}

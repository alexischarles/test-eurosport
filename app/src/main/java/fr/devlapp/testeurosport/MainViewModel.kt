package fr.devlapp.testeurosport

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.devlapp.core.model.Data
import fr.devlapp.core.model.ErrorType
import fr.devlapp.core.model.RepositoryException
import fr.devlapp.core.repository.MainRepository
import fr.devlapp.testeurosport.MainViewModel.MainViewState.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    sealed class MainViewState {
        data class MainViewData(val listData: List<Data>) : MainViewState()
        data class MainViewError(val error: RepositoryException) : MainViewState()
        object MainViewLoading : MainViewState()
    }

    private val _viewEvent = MutableStateFlow<MainViewState>(MainViewLoading)
    val viewEvent: StateFlow<MainViewState> = _viewEvent

    fun getMainData() {
        _viewEvent.value = MainViewLoading
        viewModelScope.launch {
            try {
                val mainData = MainRepository.getMainData()

                val stories = mainData.stories.sortedByDescending { it.timestampInMillis }
                val videos = mainData.videos.sortedByDescending { it.timestampInMillis }
                val listData = stories.mix(videos)

                if (listData.isNotEmpty())
                    _viewEvent.value = MainViewData(listData)
                else
                    _viewEvent.value = MainViewError(RepositoryException(ErrorType.EMPTY_ERROR))
            } catch (e: RepositoryException) {
                _viewEvent.value = MainViewError(e)
            }
        }
    }

    private fun <T> List<T>.mix(other: List<T>): List<T> {
        val first = iterator()
        val second = other.iterator()
        val list = ArrayList<T>(minOf(this.size, other.size))
        while (first.hasNext() || second.hasNext()) {
            if (first.hasNext())
                list.add(first.next())
            if (second.hasNext())
                list.add(second.next())
        }
        return list
    }
}

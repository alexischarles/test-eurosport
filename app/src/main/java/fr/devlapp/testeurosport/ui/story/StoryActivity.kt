package fr.devlapp.testeurosport.ui.story

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.skydoves.landscapist.glide.GlideImage
import fr.devlapp.core.model.Data.Story
import fr.devlapp.testeurosport.R
import fr.devlapp.testeurosport.ui.theme.White
import fr.devlapp.testeurosport.ui.views.SportLabel
import java.text.SimpleDateFormat
import java.util.*

class StoryActivity : ComponentActivity() {

    companion object {
        private const val KEY_STORY = "KEY_STORY"

        fun getIntent(context: Context, story: Story): Intent =
            Intent(context, StoryActivity::class.java).apply {
                putExtra(KEY_STORY, story)
            }
    }

    private val simpleDateFormat = SimpleDateFormat("dd/MM/yy", Locale.getDefault())

    private val story: Story?
        get() = intent.getParcelableExtra(KEY_STORY)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        story?.let { story ->
            setContent {
                ConstraintLayout() {
                    val (image, back, share, text, column) = createRefs()

                    GlideImage(
                        imageModel = story.image,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .constrainAs(image) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .aspectRatio(16f / 9f)
                    )

                    IconButton(
                        onClick = { onBackPressed() },
                        modifier = Modifier
                            .constrainAs(back) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start)
                            }
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.back),
                            contentDescription = null, // decorative element
                            tint = White,
                        )
                    }

                    IconButton(
                        onClick = { /* TODO */ },
                        modifier = Modifier
                            .constrainAs(share) {
                                top.linkTo(parent.top)
                                end.linkTo(parent.end)
                            }
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.share),
                            contentDescription = null, // decorative element
                            tint = White,
                        )
                    }

                    SportLabel(
                        text = story.sport.name.uppercase(),
                        modifier = Modifier
                            .constrainAs(text) {
                                top.linkTo(image.bottom)
                                bottom.linkTo(image.bottom)
                                start.linkTo(parent.start, 16.dp)
                            }
                    )

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .constrainAs(column) {
                                top.linkTo(text.bottom)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .padding(16.dp, 8.dp, 16.dp, 16.dp)
                    ) {
                        Text(
                            text = story.title,
                            fontSize = 18.sp,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            text = getString(R.string.author, story.author),
                            fontSize = 12.sp,
                            modifier = Modifier.padding(0.dp, 4.dp, 0.dp, 0.dp)
                        )
                        Text(
                            text = simpleDateFormat.format(story.timestampInMillis),
                            fontSize = 10.sp,
                        )
                        Text(
                            text = story.teaser,
                            fontSize = 14.sp,
                            modifier = Modifier.padding(0.dp, 8.dp, 0.dp, 0.dp)
                        )
                    }
                }
            }
        }
    }
}

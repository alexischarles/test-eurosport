package fr.devlapp.testeurosport.ui.theme

import androidx.compose.ui.graphics.Color

val White = Color(0xFFFFFFFF)
val Grey = Color(0xFFDDDDDD)
val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
package fr.devlapp.testeurosport.ui.video

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.exoplayer2.ui.StyledPlayerView
import fr.devlapp.core.model.Data.Video

class VideoActivity : ComponentActivity() {

    companion object {
        private const val KEY_VIDEO = "KEY_VIDEO"

        fun getIntent(context: Context, video: Video): Intent =
            Intent(context, VideoActivity::class.java).apply {
                putExtra(KEY_VIDEO, video)
            }
    }

    private val viewModel: VideoViewModel by viewModels()
    private val video: Video?
        get() = intent.getParcelableExtra(KEY_VIDEO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val videoPlayer = viewModel.getPlayer()

        setContent {
            AndroidView(factory = { context ->
                StyledPlayerView(context).apply {
                    player = videoPlayer
                    layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        video?.let { video ->
            viewModel.playMedia(video)
        }
    }

    override fun onPause() {
        viewModel.pauseVideo()
        super.onPause()
    }
}

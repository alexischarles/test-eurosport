package fr.devlapp.testeurosport.ui.video

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.util.EventLogger
import fr.devlapp.core.model.Data.Video

class VideoViewModel(application: Application) : AndroidViewModel(application) {

    private companion object {
        const val TAG = "VideoPlayerViewModel"
    }

    private var videoPlayer: ExoPlayer? = null
    private var currentPlaying: Video? = null

    fun getPlayer(): ExoPlayer {
        return videoPlayer ?: run {
            val context = getApplication<Application>()
            ExoPlayer.Builder(context)
                .build()
                .apply {
                    addAnalyticsListener(EventLogger())
                }
        }.also { videoPlayer = it }
    }

    fun playMedia(video: Video) {
        Log.d(TAG, "playVideo")

        if (video != currentPlaying) {
            prepareMedia(video)
        }

        videoPlayer?.playWhenReady = true
    }

    private fun prepareMedia(video: Video) {
        val mediaItem: MediaItem = MediaItem.fromUri(video.url)
        videoPlayer?.setMediaItem(mediaItem)
        videoPlayer?.prepare()

        currentPlaying = video
    }

    fun pauseVideo() {
        Log.d(TAG, "pauseVideo")
        videoPlayer?.playWhenReady = false
    }

    override fun onCleared() {
        Log.d(TAG, "onCleared")
        videoPlayer?.playWhenReady = false
        currentPlaying = null

        Log.d(TAG, "release player")
        videoPlayer?.release()
        videoPlayer = null

        super.onCleared()
    }
}

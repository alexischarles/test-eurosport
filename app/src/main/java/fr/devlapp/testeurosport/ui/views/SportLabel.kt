package fr.devlapp.testeurosport.ui.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import fr.devlapp.testeurosport.ui.theme.Purple700
import fr.devlapp.testeurosport.ui.theme.White

@Composable
fun SportLabel(text: String, modifier: Modifier) {
    Text(
        text = text.uppercase(),
        color = White,
        modifier = modifier
            .background(Purple700, RoundedCornerShape(8.dp))
            .padding(8.dp, 4.dp, 8.dp, 4.dp)
    )
}

package fr.devlapp.testeurosport.ui.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.skydoves.landscapist.glide.GlideImage
import fr.devlapp.core.model.Data.Story
import fr.devlapp.testeurosport.R
import fr.devlapp.testeurosport.ui.theme.White
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun StoryCard(story: Story, onClickStory: (Story) -> Unit) {
    val simpleDateFormat = SimpleDateFormat("dd/MM/yy", Locale.getDefault())

    ConstraintLayout(
        modifier = Modifier
            .clickable(onClick = { onClickStory(story) })
            .padding(8.dp, 4.dp, 8.dp, 4.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(White)
    ) {
        val context = LocalContext.current
        val (image, text, column) = createRefs()

        GlideImage(
            imageModel = story.image,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .constrainAs(image) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .aspectRatio(16f / 9f)
        )

        SportLabel(
            text = story.sport.name,
            modifier = Modifier
                .constrainAs(text) {
                    top.linkTo(image.bottom)
                    bottom.linkTo(image.bottom)
                    start.linkTo(parent.start, 16.dp)
                }
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(column) {
                    top.linkTo(text.bottom)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(16.dp, 8.dp, 16.dp, 16.dp)
        ) {
            Text(
                text = story.title,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
            Text(
                text = "${
                    context.getString(R.string.author, story.author)
                } - ${
                    simpleDateFormat.format(story.timestampInMillis)
                }"
            )
        }
    }
}

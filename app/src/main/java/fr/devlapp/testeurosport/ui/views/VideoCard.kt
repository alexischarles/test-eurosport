package fr.devlapp.testeurosport.ui.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.skydoves.landscapist.glide.GlideImage
import fr.devlapp.core.model.Data.Video
import fr.devlapp.testeurosport.R
import fr.devlapp.testeurosport.ui.theme.White

@Composable
fun VideoCard(video: Video, onClickVideo: (Video) -> Unit) {
    ConstraintLayout(
        modifier = Modifier
            .clickable(onClick = { onClickVideo(video) })
            .padding(8.dp, 4.dp, 8.dp, 4.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(White)
    ) {
        val context = LocalContext.current
        val (image, icon, text, column) = createRefs()

        GlideImage(
            imageModel = video.thumb,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .constrainAs(image) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .aspectRatio(16f / 9f)
        )

        Icon(
            painter = painterResource(id = R.drawable.play),
            contentDescription = null, // decorative element
            tint = White,
            modifier = Modifier
                .constrainAs(icon) {
                    top.linkTo(image.top)
                    bottom.linkTo(image.bottom)
                    start.linkTo(image.start)
                    end.linkTo(image.end)
                }
        )

        SportLabel(
            text = video.sport.name,
            modifier = Modifier
                .constrainAs(text) {
                    top.linkTo(image.bottom)
                    bottom.linkTo(image.bottom)
                    start.linkTo(parent.start, 16.dp)
                }
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(column) {
                    top.linkTo(text.bottom)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(16.dp, 8.dp, 16.dp, 16.dp)
        ) {
            Text(
                text = video.title,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
            Text(text = context.getString(R.string.views, video.views))
        }
    }
}

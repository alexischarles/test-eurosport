package fr.devlapp.testeurosport

import fr.devlapp.core.model.*
import fr.devlapp.core.repository.MainRepository
import fr.devlapp.testeurosport.MainViewModel.MainViewState.MainViewData
import fr.devlapp.testeurosport.MainViewModel.MainViewState.MainViewError
import io.mockk.coEvery
import io.mockk.mockkObject
import io.mockk.unmockkAll
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.Assert.assertEquals

@ExperimentalCoroutinesApi
class MainViewModelTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun prepare() {
            mockkObject(MainRepository)
        }

        @AfterClass
        @JvmStatic
        fun terminate() {
            unmockkAll()
        }
    }

    private val viewModel = MainViewModel()
    private val dispatcher = UnconfinedTestDispatcher()
    private val fakeSport = Sport(0, "")
    private val fakeVideo = Data.Video(0, "", "", "", 0, fakeSport, 0)
    private val fakeStory = Data.Story(0, "", "", "", 0, "", fakeSport)

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `When data is empty then get an error`() = runTest {
        coEvery {
            MainRepository.getMainData()
        }.returns(MainData(emptyList(), emptyList()))

        viewModel.getMainData()

        assertEquals(
            MainViewError(RepositoryException(ErrorType.EMPTY_ERROR)),
            viewModel.viewEvent.value
        )
    }

    @Test
    fun `When throw an exception then get an error`() = runTest {
        coEvery {
            MainRepository.getMainData()
        }.throws(RepositoryException(ErrorType.NO_NETWORK_ERROR))

        viewModel.getMainData()

        assertEquals(
            MainViewError(RepositoryException(ErrorType.NO_NETWORK_ERROR)),
            viewModel.viewEvent.value
        )
    }

    @Test
    fun `Result is videos and stories mixed`() = runTest {
        coEvery {
            MainRepository.getMainData()
        }.returns(MainData(listOf(fakeVideo), listOf(fakeStory, fakeStory)))

        viewModel.getMainData()

        assertEquals(
            MainViewData(listOf(fakeStory, fakeVideo, fakeStory)),
            viewModel.viewEvent.value
        )
    }

    @Test
    fun `Videos are sorted by date`() = runTest {
        val video1 = fakeVideo.copy(timestampInMillis = 1)
        val video2 = fakeVideo.copy(timestampInMillis = 2)
        coEvery {
            MainRepository.getMainData()
        }.returns(MainData(listOf(video1, video2), emptyList()))

        viewModel.getMainData()

        assertEquals(
            MainViewData(listOf(video2, video1)),
            viewModel.viewEvent.value
        )
    }

    @Test
    fun `Stories are sorted by date`() = runTest {
        val story1 = fakeStory.copy(timestampInMillis = 1)
        val story2 = fakeStory.copy(timestampInMillis = 2)
        coEvery {
            MainRepository.getMainData()
        }.returns(MainData(emptyList(), listOf(story1, story2)))

        viewModel.getMainData()

        assertEquals(
            MainViewData(listOf(story2, story1)),
            viewModel.viewEvent.value
        )
    }
}

package fr.devlapp.core.model

import android.os.Parcelable
import fr.devlapp.core.model.Data.Story
import fr.devlapp.core.model.Data.Video
import fr.devlapp.core.repository.apimodel.ApiMainData
import fr.devlapp.core.repository.apimodel.ApiSport
import fr.devlapp.core.repository.apimodel.ApiStory
import fr.devlapp.core.repository.apimodel.ApiVideo
import kotlinx.parcelize.Parcelize

@Parcelize
data class MainData(
    val videos: List<Video>,
    val stories: List<Story>,
) : Parcelable {

    internal constructor(
        apiMainData: ApiMainData
    ) : this(
        videos = apiMainData.videos?.map { Video(it) } ?: emptyList<Video>(),
        stories = apiMainData.stories?.map { Story(it) } ?: emptyList<Story>(),
    )
}

sealed class Data : Parcelable {

    @Parcelize
    data class Video(
        val id: Int,
        val title: String,
        val thumb: String,
        val url: String,
        val timestampInMillis: Long,
        val sport: Sport,
        val views: Int,
    ) : Data(), Parcelable {

        internal constructor(
            apiVideo: ApiVideo
        ) : this(
            id = apiVideo.id,
            title = apiVideo.title,
            thumb = apiVideo.thumb,
            url = apiVideo.url,
            timestampInMillis = apiVideo.date.toLong() * 1000,
            sport = Sport(apiVideo.sport),
            views = apiVideo.views,
        )
    }

    @Parcelize
    data class Story(
        val id: Int,
        val title: String,
        val teaser: String,
        val image: String,
        val timestampInMillis: Long,
        val author: String,
        val sport: Sport,
    ) : Data(), Parcelable {

        internal constructor(
            apiStory: ApiStory
        ) : this(
            id = apiStory.id,
            title = apiStory.title,
            teaser = apiStory.teaser,
            image = apiStory.image,
            timestampInMillis = apiStory.date.toLong() * 1000,
            author = apiStory.author,
            sport = Sport(apiStory.sport),
        )
    }
}

@Parcelize
data class Sport(
    val id: Int,
    val name: String,
) : Parcelable {

    internal constructor(
        apiSport: ApiSport
    ) : this(
        id = apiSport.id,
        name = apiSport.name,
    )
}

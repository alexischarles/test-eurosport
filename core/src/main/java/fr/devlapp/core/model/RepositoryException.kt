package fr.devlapp.core.model

data class RepositoryException(
    val type: ErrorType,
    override val cause: Throwable? = null,
) : Exception()

enum class ErrorType {
    NO_NETWORK_ERROR,
    TIMEOUT_ERROR,
    PARSING_ERROR,
    EMPTY_ERROR,
    UNKNOWN_ERROR
}

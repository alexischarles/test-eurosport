package fr.devlapp.core.repository

import android.util.Log
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonEncodingException
import fr.devlapp.core.model.ErrorType
import fr.devlapp.core.model.RepositoryException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.EOFException
import java.io.InterruptedIOException
import java.net.UnknownHostException

open class BaseRepository {

    @Throws(RepositoryException::class)
    internal suspend fun <T : Any> genericApiCall(
        call: suspend () -> Response<T>,
        ioDispatcher: CoroutineDispatcher,
    ): T {
        return withContext(ioDispatcher) {
            try {
                val response = call.invoke()

                response.body()
                    ?.takeIf { response.isSuccessful }
                    ?.let { return@withContext it }

                throw RepositoryException(ErrorType.UNKNOWN_ERROR)
            } catch (exception: Exception) {
                if (exception !is UnknownHostException) {
                    Log.e(
                        "MainRepository",
                        "Call throw Exception on getMainData",
                        exception
                    )
                }
                throw when (exception) {
                    is RepositoryException -> exception
                    is EOFException,
                    is JsonEncodingException,
                    is JsonDataException ->
                        RepositoryException(ErrorType.PARSING_ERROR, cause = exception)
                    is InterruptedIOException ->
                        RepositoryException(ErrorType.TIMEOUT_ERROR, cause = exception)
                    is UnknownHostException ->
                        RepositoryException(ErrorType.NO_NETWORK_ERROR, cause = exception)
                    else ->
                        RepositoryException(ErrorType.UNKNOWN_ERROR, cause = exception)
                }
            }
        }
    }
}

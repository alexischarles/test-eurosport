package fr.devlapp.core.repository

import android.util.Log
import fr.devlapp.core.model.MainData
import fr.devlapp.core.repository.api.MainApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object MainRepository : BaseRepository() {

    private var defaultDispatcher: CoroutineDispatcher = Dispatchers.Default
    private var ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    suspend fun getMainData(): MainData =
        withContext(defaultDispatcher) {
            Log.d("MainRepository", "Request getMainData")

            val apiMainData = genericApiCall(
                call = { RetrofitClient.instance.create(MainApi::class.java).getMainData() },
                ioDispatcher = ioDispatcher
            )


            return@withContext MainData(apiMainData)
        }
}

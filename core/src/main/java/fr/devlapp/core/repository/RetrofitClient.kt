package fr.devlapp.core.repository

import com.squareup.moshi.Moshi
import fr.devlapp.core.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

const val HTTP_TIMEOUT_S = 5L

object RetrofitClient {

    private const val baseUrl: String = BuildConfig.BASE_URL
    private val cacheSize: Long = BuildConfig.NETWORK_CACHE_SIZE

    /*val cache: Cache? by lazy {
        Cache(File(appCacheDir, "api"), cacheSize)
    }*/

    private val moshi: Moshi by lazy {
        Moshi.Builder()
            .build()
    }

    val instance: Retrofit by lazy {
        RetrofitBuilder(baseUrl).build()
    }

    private class RetrofitBuilder(baseUrl: String) {

        private val httpBuilder: OkHttpClient.Builder = OkHttpClient.Builder().apply {
            //cache(cache)
            callTimeout(HTTP_TIMEOUT_S, TimeUnit.SECONDS)
            connectTimeout(HTTP_TIMEOUT_S, TimeUnit.SECONDS)
            readTimeout(HTTP_TIMEOUT_S, TimeUnit.SECONDS)
        }

        val retrofitBuilder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi))

        fun build(): Retrofit = retrofitBuilder.client(httpBuilder.build()).build()
    }
}

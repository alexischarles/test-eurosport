package fr.devlapp.core.repository.api

import fr.devlapp.core.repository.apimodel.ApiMainData
import retrofit2.Response
import retrofit2.http.GET

internal interface MainApi {

    @GET("json-storage/bin/edfefba")
    suspend fun getMainData(): Response<ApiMainData>
}

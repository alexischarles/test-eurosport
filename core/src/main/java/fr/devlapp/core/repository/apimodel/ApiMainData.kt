package fr.devlapp.core.repository.apimodel

internal data class ApiMainData(
    val videos: List<ApiVideo>?,
    val stories: List<ApiStory>?,
)

internal data class ApiVideo(
    val id: Int,
    val title: String,
    val thumb: String,
    val url: String,
    val date: Float,
    val sport: ApiSport,
    val views: Int,
)

internal data class ApiStory(
    val id: Int,
    val title: String,
    val teaser: String,
    val image: String,
    val date: Float,
    val author: String,
    val sport: ApiSport,
)

internal data class ApiSport(
    val id: Int,
    val name: String,
)
